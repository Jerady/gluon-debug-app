package de.jensd.gluon.debug;

import com.guigarage.responsive.ResponsiveHandler;
import com.sun.glass.ui.Screen;
import java.util.Locale;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class App extends Application {

    private DebugView debugView;

    @Override
    public void init() throws Exception {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Override
    public void start(Stage primaryStage) {
        System.out.print("Starting Gluon Debug View. Please wait....");

        debugView = new DebugView();
        Scene scene = new Scene(debugView, 500.0, 500.0, Color.BLACK);
        String css = getClass().getResource("style.css").toExternalForm();
        scene.getStylesheets().addAll(css);
        primaryStage.setTitle("Gluon Debug");
        primaryStage.setScene(scene);
        primaryStage.show();
        debugView.log("App started");

        scene.widthProperty().addListener((observable, oldValue, newValue)
                -> updateSizeInformation(primaryStage, scene));
        primaryStage.widthProperty().addListener((observable, oldValue, newValue)
                -> updateSizeInformation(primaryStage, scene)
        );
        primaryStage.getScene().heightProperty().addListener((observable, oldValue, newValue)
                -> updateSizeInformation(primaryStage, scene)
        );

        updateSizeInformation(primaryStage, scene);

        ResponsiveHandler.addResponsiveToWindow(primaryStage);
        ResponsiveHandler.setOnDeviceTypeChanged((observable,  oldValue,  newValue) -> {
            debugView.getResponsiveDeviceTypeLabel().setText(newValue.name());
        });
    }

    private void updateSizeInformation(Stage stage, Scene scene) {
        debugView.getScreenSizeLabel().setText(getScreenSizeInfo());
        debugView.getPrimaryStageSizeLabel().setText(getPrimaryStageSizeInfo(stage));
        debugView.getSceneSizeLabel().setText(getSceneSizeInfo(scene));
        debugView.log("ScreenSize: " + getScreenSizeInfo());
        debugView.log("StageSize: " + getPrimaryStageSizeInfo(stage));
        debugView.log("SceneSize: " + getSceneSizeInfo(scene));

    }

    private String getScreenSizeInfo() {
        return String.format("%d x %d", Screen.getMainScreen().getWidth(), Screen.getMainScreen().getHeight());
    }

    private String getPrimaryStageSizeInfo(Stage stage) {
        return String.format("%.00f x %.00f", stage.getWidth(), stage.getHeight());
    }

    private String getSceneSizeInfo(Scene scene) {
        return String.format("%.00f x %.00f", scene.getWidth(), scene.getHeight());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.setProperty("prism.text", "t2k");
        System.setProperty("prism.lcdtext", "true");
        launch(args);
    }
}
