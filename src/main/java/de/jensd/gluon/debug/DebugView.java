package de.jensd.gluon.debug;

import com.gluonhq.charm.down.common.Platform;
import com.gluonhq.charm.down.common.PlatformFactory;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters
 */
public class DebugView extends VBox {

    @FXML
    private Label screenSizeLabel;
    @FXML
    private Label primaryStageSizeLabel;
    @FXML
    private Label responsiveDeviceTypeLabel;
    @FXML
    private Label sceneSizeLabel;
    @FXML
    private TabPane mainTabPane;
    @FXML
    private ResourceBundle resources;
    @FXML
    private TextArea logTextArea;

    public DebugView() {
        init();
    }

    private void init() {

        PlatformFactory.getPlatform().setOnLifecycleEvent((Platform.LifecycleEvent param) -> {
            switch (param) {
                case START:
                    break;
                case PAUSE:
                    break;
                case RESUME:
                    break;
                case STOP:
                    break;
            }
            return null;
        });

        ResourceBundle resourceBundle = ResourceBundle.getBundle(getClass().getPackage().getName() + ".debugview");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("debugview.fxml"));
        fxmlLoader.setResources(resourceBundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Label getSceneSizeLabel() {
        return sceneSizeLabel;
    }

    public Label getScreenSizeLabel() {
        return screenSizeLabel;
    }

    public Label getPrimaryStageSizeLabel() {
        return primaryStageSizeLabel;
    }

    public Label getResponsiveDeviceTypeLabel() {
        return responsiveDeviceTypeLabel;
    }

    public void log(String message) {
        logTextArea.appendText(message + "\n");
    }

    /*
     * -------------------------- ACTIONS -------------------------- 
     */
    @FXML
    public void onNextTab() {
        int index = mainTabPane.getSelectionModel().getSelectedIndex();
        if (index == mainTabPane.getTabs().size()) {
            return;
        }
        mainTabPane.getSelectionModel().select(index++);
    }

    @FXML
    public void onPreviousTab() {
        int index = mainTabPane.getSelectionModel().getSelectedIndex();
        if (index == 0) {
            return;
        }
        mainTabPane.getSelectionModel().select(index--);
    }

}
